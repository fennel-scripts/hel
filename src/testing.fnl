#!/usr/bin/env fennel
(local fennel (require :fennel))
(local view fennel.view)
(local tcat table.concat)
(local tunpack table.unpack)
(local tinsert table.insert)


(local utils (require :lambda.utils))
;; (local plates (require :plates))


;; (local mem (plates:new {:name :mem}))
;; (local stack (plates:new {:name :stack}))
;; (stack:push 10)
;; (stack:view)
;; (stack:push 15)
;; (stack:view)

;; (mem:push 100)
;; (stack:view)
;; (mem:view)
;; (mem:push (stack:pop))
;; (stack:view)
;; (mem:view)
;; (stack:push 2)
;; (stack:view)

;;(macro using [a b args]
;;  (match (. args 1)
;;	:a `(require a)
;;	:b `(require b)
;;	))
;;(macrodebug (using "repl" "helheim" ["a"]))

(local debug (require :helheim.dbg))
(local dbg (debug.new))
(dbg:open)
;;(print (view dbg))
(dbg:echo "hello world")



(dbg:end)
