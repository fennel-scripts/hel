#!/usr/bin/env fennel
(local fennel (require :fennel))
(local view fennel.view)
(local tcat table.concat)
(local tunpack table.unpack)
(local tinsert table.insert)
(var dodebug false)


;;(when (= (. arg 2) "--dbg")  (set dodebug true))
;;(local debug (require :helheim.debug))
;;(print (view arg))

;; (local x (. arg 0))
;; (local path (string.char)
;; (print "path" path)
;; (os.exit)


(local utils (require :lambda.utils))
(local fil (require :fil))
(local plates (require :helheim.plates))
(local hel (require :helheim.hel))
;;(print (fennel.searchModule :helheim.plates))

;;(each [key value (pairs prog.lines)] (print "DBG" (fennel.view value)))
;;(lambda next [stack ])
(local tape (let [prog (fil.init {:name (or (. arg 1) :main.hel)})] prog.lines))

(lambda split [input ?sep]
  (let [sep (or ?sep "%s")]
	(let [result {}
		  matches (string.gmatch input (.. "([^" sep "]+)"))]
	  (each [str matches]
		(tinsert result 1 (or (tonumber str) str))) result)))




(each [key value (ipairs tape)]
  (tset tape key (split value))
  ;;(print (view (split value)))
  )

;;(print (view tape))

(local prog (hel.new "" tape))
;;(set prog.dbg (: (debug.new) :open))


;;(print (view prog))
(prog:read-tape tape)
;;(prog:eval)

;;(stack:view)
;;(prog.dbg:close)
