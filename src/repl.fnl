#!/usr/bin/env fennel
(local fennel (require :fennel))
(local view fennel.view)
(local tcat table.concat)
(local tunpack table.unpack)
(local tinsert table.insert)

(local utils (require :lambda.utils))
(local fil (require :fil))
(local f fil)
(local plates (require :lib.plates))
(local hel (require :lib.hel))
;;(each [key value (pairs prog.lines)] (print "DBG" (fennel.view value)))
;;(lambda next [stack ])

(local tape [])
(lambda split [input ?sep]
  (let [sep (or ?sep "%s")]
 	(let [result {}
 		  matches (string.gmatch input (.. "([^" sep "]+)"))]
 	  (each [str matches] (tinsert result 1 (or (tonumber str) str))) result)))


;; (each [key value (ipairs tape)] (tset tape key (split value)))
;; (print (view tape))


;;(hel.read-tape tape)
;;(local helheim (hel.init))
(while [(local inp (io.read))]
  (let [input (split inp)]
	(hel.eval (plates:new {:stack input}))
	;;(hel.eval )
	))

   (each [key value (ipairs tape)]
   	(let [line (plates:new {:name (.. "line-" key) :stack value})]
   	  ;;(print line.name (view line.stack))
   	  (hel.parse line key)
   	  ;;(print line.name (view line.stack))
   	  (print (view mem.stack ) (view stack.stack)))
   )


;;(stack:view)
