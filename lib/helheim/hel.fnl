#!/usr/bin/env fennel
(local fennel (require :fennel))
(local utils (require :lambda.utils))
(local fil (require :fil))

(local plates (require :helheim.plates))

(local fort (require :fort))

(local view fennel.view)
(local tcat table.concat)
(local tunpack table.unpack)
(local tinsert table.insert)
(local lumber (require :lumber))



(local logfile (io.open (.. (os.getenv "HOME")
							"/.logs/helheim.log") :w+))

(fn fmtr [level input context]
  (tcat ["[" level.name "]" "\n" input ])
  )



(local logger
	 (lumber.new
	  {:filter fennel.view ;;(require :inspect)
       ;;:format fmtr
	   :format (require :lumber.format.file)
	   :level lumber.levels.INFO
       :out logfile ;;io.stderr
       ;;:separator "\n"
	   :separator " "
	   }))


(local hel {})
(set hel.logger
	 (lumber.new
	  {:filter fennel.view
	   :format (require :lumber.format.term)
	   :level lumber.levels.INFO
	   :out io.stderr
	   :separator " "}))

(lambda hel.add [stack args]
  (hel.logger:debug "adding ")
  (faccumulate [n 0 i 1 args]
    (+ n (or (stack:pop) 0))))

(lambda hel.subtract [stack args]
  (hel.logger:debug "subtracting ")
  (faccumulate [n 0 i 1 args]
	(- n (stack:pop))))

(lambda hel.divide [stack args]
  (hel.logger:debug "dividing ")
  (math.floor
   (faccumulate [n 0 i 1 args]
	 (when (not= (stack:peek) 0)
	   (/ n (stack:pop))))))

(lambda hel.multiply [stack args]
  (hel.logger:debug "multiplying ")
  (math.floor
   (faccumulate [n 0 i 1 args]
	 (* n (stack:pop)))))

(lambda hel.compare [stack] (= (stack:pop) (stack:pop)))

(lambda hel.noop [...] ...)
(lambda hel.reserved [...] (logger:warn "used unimplemented function " ...))
(lambda hel.deprecated [name]
  (logger:warn "used deprecated function " name))

(lambda hel.noop* [...]
  (when ...
   (let [args [...]]
	(print (view (or args ""))))))


(lambda hel.parse [prog line ?lino]
  (let [wmem prog.wmem
		mem prog.mem
		stack prog.stack
		noop hel.noop
		deprecated hel.deprecated
		reserved hel.reserved
		]
	
	(local command (line:pop))
	(match command
	  ;;comments
	  :#	(noop line.stack)
	  ";"	(noop line.stack)
	  ";;"	(noop line.stack)
	  ";;;" (noop line.stack)
	  )

	;;> pop something from somewhere
	;;< push something from somewhere to somewhere
	;;default input is 'line'
	;;'line' is a stack
	;;starting stack is called 'stack'
	;;& points to that 'stack'
	;;% points to the 'mem' stack
	;;'wmem' always points to the currently selected stack
	;;'wmem' can be changed using [&/%] on its own line
	;;! points to standard out, or general output(used to output to the screen)
	;;. points to standard in
	;;_ points to 'line' when it can not be default
	;;* is used for logger functions
	;;
	
   
	(match command
	  ;;stack-utils
	  :>	(wmem:pop)
	  :<	(wmem:push (line:pop))
	  :<_	(wmem:push (line:pop))
	  :<&	(wmem:push (stack:pop))
	  :<%	(wmem:push (mem:pop))
	  :><	(wmem:restack!)
	  :>>	(wmem:view)
	  :%	(set prog.wmem prog.stack)
	  :&	(set prog.wmem prog.mem)
	  ;; printing to screen
	  :>!	(print ">>" (line:pop))
	  :!>	(do (deprecated "!>") (print ">>" (line:pop)))
	  :>!!	(do (for [i 1 (line:pop) 1]
	   			  (io.write (.. " " (wmem:pop)))) (print ""))
	  ;; logging
	  :*	(reserved :* line.stack)
	  :*<	(set hel.logger.level (tonumber (line:pop)))
	  :*!	(hel.logger (wmem:restack))
	  :*!!	(hel.logger (stack:restack) (mem:restack))
	  :*&%!	(hel.logger (stack:restack) (mem:restack))
	  :*%&!	(hel.logger (mem:restack) (stack:restack))
	  :*!_  (hel.logger line.stack)
	  :*!&	(hel.logger (stack:restack))
	  :*!%	(hel.logger (mem:restack))
	  ;;math
	  :-	(wmem:push (hel.subtract	wmem	(line:pop)))
	  :*	(wmem:push (hel.multiply	wmem	(line:pop)))
	  :/	(wmem:push (hel.divide		wmem	(line:pop)))
	  :+	(wmem:push (hel.add			wmem	(line:pop)))
	  ;;logic
	  :?	(if (hel.compare wmem)
			 (wmem:push (line:pop))
			 (do (line:pop)
				 ;;(if line)
				 (wmem:push (or (line:pop) 0))
				 ))
	  :<!.	(wmem:push (io.read))
	  )
	(match command
	  ;;tbd not yet implemented but "reserved" keywords
	  :->	(reserved :->	line.stack)
	  :-->	(reserved :-->	line.stack)
	  :=>	(reserved :=>	line.stack)
	  :==>	(reserved :==>	line.stack)
	  :<-	(reserved :<-	line.stack)
	  :<--	(reserved :<--	line.stack)
	  :<=	(reserved :<=	line.stack)
	  :<==	(reserved :<==	line.stack)
	  )
	(match command
	  ;; ending the program
	  :quit	(os.exit)
	  :exit	(os.exit)
	  ":q"	(os.exit)
	  )
	)
  )

(lambda hel.new [name tape]
  (local mem (plates:new {:name "mem"}))
  (local stack (plates:new {:name "stack"}))
  (logger:debug "empty stack and mem created")
  
  {:eval hel.eval
   :read-tape hel.read-tape
   :parse hel.parse
   : tape
   :mem mem
   :stack stack
   :wmem stack}
  

  )

(lambda hel.read-tape [prog]
  (logger  prog.mem)
  (logger  prog.stack)
  (each [key value (ipairs prog.tape)]
	(let [line (plates:new {:name (.. "[" key "]") :stack value})]
	  ;;(print line.name (view line.stack))
	  ;;(print line.name (view line.stack))
	  (prog:eval line)
	  )))

(lambda hel.eval [prog line ?key]
  (logger line.stack)
  (prog:parse line ?key)
  ;;(log.print prog)
  ;;(logger "mem" prog.mem.stack)
  ;;(logger "stack" prog.stack.stack)

  (logger  prog.mem)
  (logger  prog.stack)
  )


hel
