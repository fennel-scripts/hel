#!/usr/bin/env fennel
(local tcat table.concat)
(local fennel (require :fennel))
(local view fennel.view)
(fn tprint [...] " table-printing function " (print (tcat ...)))
(fn pp [...] "pretty-printing function" (print (view ...)))
(local reg {})




(lambda reg.new [this {:name ?name :registry ?registry}]
  (let [obj {:name (or ?name :registry) :_registry (or ?registry {})}]
	(setmetatable obj this)
	(set this.__index this)
	obj))

(lambda reg.set [this key value]
  (tset this._registry key value)
  (pp (this:get))
  this
  )
(lambda reg.get [this ?key]
  (if ?key
	  (. this :_registry ?key)
	  (. this :_registry)
	  )
  )



(fn main []
  (local reggy (reg:new {:name "reggy" :registry {:foo "bar"}}))
  (pp reggy._registry)
  (reggy:set :lorem "ipsum" )
  )
(main)

;;reg
